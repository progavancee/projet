package fr.unice.miage.classloader;

import fr.unice.miage.plugins.IPluginVue;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/*
 * Charge les .class et les .jar (pas les .zip pour l'instant) 
 * 
 */
public class MyClassLoader extends SecureClassLoader {

    private ArrayList<File> path = new ArrayList<File>();

    /**
     * 
     * @params : Tableau de chemins contenant les plugins à charger
     */
    public MyClassLoader(String[] s){
        File f ;
        for (String chemin : s){
            f=new File(chemin);
            if (f.isDirectory() ||f.getAbsolutePath().endsWith(".jar")||f.getAbsolutePath().endsWith(".zip")) {
                path.add(f);
            }
        }
    }


    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] b = loadClassData(name);

        return super.defineClass(name,b,0,b.length);
    }

    private byte[] loadClassData(String name) throws ClassNotFoundException {
    	
        if(path.isEmpty()) throw new NullPointerException();
        byte[] b = null;

        for (File dir : path){

            if(dir.isDirectory()) {
                String s = getPathFile(dir.getAbsolutePath(), name);
                File file = new File(s);
                if (file.exists())
                    b = new byte[(int) file.length()];
                FileInputStream fileInputStream = null;
                try {
                    fileInputStream = new FileInputStream(file);
                    fileInputStream.read(b);//read() n'est pas une bonne methode pour remplir le tableau (c.f Huet)
                    fileInputStream.close();
                    break;
                } catch (FileNotFoundException e) {
                    System.out.println("File not found");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.out.println("Error reading the file");
                    e.printStackTrace();
                }
            }
            else if(dir.getAbsolutePath().endsWith(".jar")){
                JarFile file;
                try {
                    file = new JarFile(dir.toString());
                    for (Enumeration<JarEntry> enu = file.entries(); enu.hasMoreElements();) {
                        JarEntry entry = enu.nextElement();
                        if (entry.getName().endsWith(name+".class")){
                            b = new byte[(int) entry.getSize()];
                        	 BufferedInputStream is = new BufferedInputStream(file.getInputStream(entry));
                        	 is.read(b);
                        	 is.close();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            else if(dir.getAbsolutePath().endsWith(".zip")){
            	ZipFile zipFile = null;
				try {
					zipFile = new ZipFile(dir.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
            	Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while(entries.hasMoreElements()){
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().endsWith(name+".class")){
                        b = new byte[(int) entry.getSize()];
                    	 BufferedInputStream is;
						try {
							is = new BufferedInputStream(zipFile.getInputStream(entry));
							is.read(b);
	                    	is.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
                    	 
                    }
                }
            }
        }

        return b;
    }

    private String getPathFile(String dir,String name){
        String path = dir.concat("/").concat(name).replace(".","/");
        return path.concat(".class");
    }

    //le main est juste pour tester (à supprimer)
    public static void main(String[] args) {

        String[] chemins = {System.getProperty("user.dir")+"/PluginsACharger/Archive.zip"};        

        MyClassLoader c = new MyClassLoader(chemins);
        
        try {
            Class<?> myPlugin = c.loadClass("SimpleModule");
            if (myPlugin != null){
                ((IPluginVue) myPlugin.newInstance()).plug();
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

}