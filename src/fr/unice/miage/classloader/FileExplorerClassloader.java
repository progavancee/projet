package fr.unice.miage.classloader;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.unice.miage.plugins.IPluginVue;


/*
 * Charge tout les fichiers .class, .jar et .zip contenus dans un repertoire
 */
public class FileExplorerClassloader {

    private File base;
    private MyClassLoader loader;
    private HashMap<String,Class<?>> listeClass;
    
    public FileExplorerClassloader(File base) {
        this.base=base; 
        listeClass = (new HashMap<String,Class<?>>());
        loader = new MyClassLoader(new String[]{base.getAbsolutePath()});
    }
    
    /**
     * 
     * @return Liste des classes chargees
     */
	public HashMap<String,Class<?>> getListeClass() {
		return listeClass;
	}

	/**
	 * Charge tous les plugins d'un repertoire selon son type
	 * @param type Le type de plugins a charger
	 * @return une liste de class contenant les plugins
	 */
	public ArrayList<Class<?>> loadPlugins(Class<?> type) {

		ArrayList<Class<?>> loadedPlugin = new ArrayList<Class<?>>();
		
		for(Entry<String, Class<?>> e : getListeClass().entrySet()) {
            Class<?> plugin = loadPlugin(type, e.getKey());
			if (plugin != null)
				System.out.println("Chargement de la classe " +e.getKey());
				loadedPlugin.add(plugin);
		}
				
		return loadedPlugin;
	}
	
	/**
	 * Charge un seul plugin en fonction de son nom et de son type
	 * @param type le type du plugin (ex : IPluginVue.class)
	 * @param pluginName le nom du plugin 
	 * @return un class du plugin charge
	 */
	public Class<?> loadPlugin(Class<?> type, String pluginName) {
		
		Class<?> pluginClass = getListeClass().get(pluginName);
		

		int modifiers = pluginClass.getModifiers();
		if (Modifier.isAbstract(modifiers)
				|| Modifier.isInterface(modifiers)) {
			System.out.println("n'est pas une classe");
			return null;
		}
				
		if (pluginClass != null && type.isAssignableFrom(pluginClass)) {
			return pluginClass;
		}
		return null;
	}
	
	public void loadClasses(File path){
		Class<?> currentClass = null;
		try {
			currentClass = loader.loadClass(path.getName().replaceAll(".class", ""));
		} catch (ClassNotFoundException e) {
						e.printStackTrace();
		}
        getListeClass().put(currentClass.getName(),currentClass);
	}
	
	public void loadJar(File path){
		JarFile file = null;
		MyClassLoader mcl = new MyClassLoader(new String[]{path.getAbsolutePath()});
            try {
				file = new JarFile(path.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
            for (Enumeration<JarEntry> enu = file.entries(); enu.hasMoreElements();) {
                JarEntry entry = enu.nextElement();
                if (entry.getName().endsWith(".class")){
	                Class<?> currentClass = null;
					try {
						currentClass = mcl.loadClass(entry.getName().replaceAll(".class", ""));
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
			        getListeClass().put(currentClass.getName(),currentClass);
                }
            } 
	}
	
	public void loadZip(File path) {
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(path.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		};
		Enumeration<? extends ZipEntry> entries = zipFile.entries();
		MyClassLoader mcl = new MyClassLoader(new String[]{path.getAbsolutePath()});
	    while(entries.hasMoreElements()){
	        ZipEntry entry = entries.nextElement();
	        if (entry.getName().endsWith(".class")){
                Class<?> currentClass = null;
				try {
					currentClass = mcl.loadClass(entry.getName().replaceAll(".class", ""));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
		        getListeClass().put(currentClass.getName(),currentClass);
            }
	    }
	}
	
    public HashMap<String,Class<?>> load(){
	        if (base.isDirectory()){
	            File[] files=base.listFiles();
	            for (File path : files){
	            	if(path.getName().endsWith(".class")){
		                loadClasses(path);
	            	}
	            	else if(path.getName().endsWith(".jar")){
	            		loadJar(path);
	            	}
	            	else if(path.getName().endsWith(".zip")){
	            		 loadZip(path);    
	            	}
	            }
	        }
	        else if (base.getPath().endsWith(".jar")){
	        	loadJar(base);
	        }
	        else if (base.getPath().endsWith(".zip")){
	        	loadZip(base);
	        }
        return getListeClass();
    }

    //le main est juste pour tester (à supprimer)
    public static void main(String[] args) {
    	FileExplorerClassloader r = new FileExplorerClassloader(new File(System.getProperty("user.dir")+"/PluginsACharger/module.jar"));

        r.load();
       
        System.out.println("Classe(s) chargee(s) :");
        for(Entry<String, Class<?>> e : r.getListeClass().entrySet()) {
            System.out.println(e.getKey()+" | "+e.getValue());
        }
        
        //Exemple chargement de classe avec le plugin correspondant
        Class<?> lePlugin = r.loadPlugins(IPluginVue.class).get(0);
        System.out.println("lePlugin : "+lePlugin);
        
    }




}