package fr.unice.miage.prog.interfaces;

import javax.swing.ImageIcon;

public class ContexteFichierColonneNom {
	
	private ITypeImageFichier typeImageFichier;

	public ContexteFichierColonneNom(ITypeImageFichier typeImageFichier) {
		super();
		this.typeImageFichier = typeImageFichier;
	}


	public ImageIcon getImage()
	{
		return typeImageFichier.getImage();
	}


	public ITypeImageFichier getTypeImageFichier() {
		return typeImageFichier;
	}


	public void setTypeImageFichier(ITypeImageFichier typeImageFichier) {
		this.typeImageFichier = typeImageFichier;
	}
	
	

}
