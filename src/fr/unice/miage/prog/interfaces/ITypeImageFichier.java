package fr.unice.miage.prog.interfaces;

import javax.swing.ImageIcon;

import fr.unice.miage.proga.explorateur.ihm.LabelPersonnalise;

public interface ITypeImageFichier {
  ImageIcon getImage();
}
