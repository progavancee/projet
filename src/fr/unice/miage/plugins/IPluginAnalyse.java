package fr.unice.miage.plugins;

import javax.swing.JFrame;

import fr.unice.miage.proga.explorateur.ihm.Fenetre;

public interface IPluginAnalyse {
	
	
	
	public void plug();
	public void unplug();
	public String getName();
	public String getDescription();
    //etat du plugin : active/desactive
    public boolean getEtat();
    //active/desactive le plugin
    public void activateIt();
    public void desactivateIt();
	 
	
	
	//Cette methode peut permettre de rajouter a l'application principale des composants ou autres en plus
	public void addPlugin(JFrame fenetre);
}
