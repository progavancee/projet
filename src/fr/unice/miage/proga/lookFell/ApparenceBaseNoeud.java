package fr.unice.miage.proga.lookFell;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class ApparenceBaseNoeud extends DefaultTreeCellRenderer{

	   private Icon iconNoeud;

	    public ApparenceBaseNoeud(Icon icon) {
	        this.iconNoeud = icon;
	    }

	
		@Override
		 public Component getTreeCellRendererComponent(
                  JTree tree,
                 Object value,
                 boolean sel,
                 boolean expanded,
                 boolean leaf,
                 int row,
                 boolean hasFocus) 
		{

		 super.getTreeCellRendererComponent(
		                 tree, value, sel,
		                 expanded, leaf, row,
		                 hasFocus);
		 if (leaf) {
		     setIcon(iconNoeud);
		    
		 } 
		 return this;
   } 
}
