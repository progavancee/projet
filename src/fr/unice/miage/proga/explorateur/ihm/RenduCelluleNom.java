package fr.unice.miage.proga.explorateur.ihm;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.DefaultTableCellRenderer;

public class RenduCelluleNom extends DefaultTableCellRenderer{

	 public void fillColor(JTable t,LabelPersonnalise l,boolean isSelected ){
	        //setting the background and foreground when JLabel is selected
	        if(isSelected){
	            l.setBackground(t.getSelectionBackground());
	            l.setForeground(t.getSelectionForeground());
	        }
	 
	        else{
	            l.setBackground(t.getBackground());
	            l.setForeground(t.getForeground());
	        }
	 
	    }
	 
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
	         boolean hasFocus, int row, int column)
	     {
	    	
	 
	        if(value instanceof LabelPersonnalise)
	        {
	        	LabelPersonnalise label = (LabelPersonnalise)value;
	            //to make label foreground n background visible you need to
	            // setOpaque -> true
	            label.setIcon(label.getContexteFichierColonneNom().getImage());
	            label.setOpaque(true);
	            fillColor(table,label,isSelected);
	            return label;
	 
	        }
	 
	        else
	            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	     }

}
