/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unice.miage.proga.explorateur.ihm;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

/**
 *
 * @author michel
 */
public class JTablePanelDroit extends JTable{

    private TableModelExplorer tableModelExplorer;
    
    public JTablePanelDroit(TableModelExplorer tableModelExplorer) {
        super(tableModelExplorer);
        this.tableModelExplorer = tableModelExplorer;
        //this.setFillsViewportHeight(true);
    }    
}
