package fr.unice.miage.proga.explorateur.ihm;

import javax.swing.Icon;
import javax.swing.JLabel;

import fr.unice.miage.prog.interfaces.ContexteFichierColonneNom;

public class LabelPersonnalise extends JLabel {

	private String typeFichier;
	
	private ContexteFichierColonneNom contexteFichierColonneNom;
	
	private String path;
	
	public LabelPersonnalise(String nomDossier, String typeFichier, ContexteFichierColonneNom contexte,String location)
	{
		super(nomDossier);
		this.typeFichier = typeFichier;
		this.contexteFichierColonneNom = contexte;
		this.path=location;
		
	}
	

	public String getPath() {
		return path;
	}


	public String getTypeFichier() {
		return typeFichier;
	}

	public void setTypeFichier(String typeFichier) 
	{
		this.typeFichier = typeFichier;
	}

	public ContexteFichierColonneNom getContexteFichierColonneNom() {
		return contexteFichierColonneNom;
	}

	public void setContexteFichierColonneNom(ContexteFichierColonneNom contexteFichierColonneNom) {
		this.contexteFichierColonneNom = contexteFichierColonneNom;
	}

	@Override
	public String toString() {
		return "LabelPersonnalise [typeFichier=" + typeFichier + ", contexteFichierColonneNom="
				+ contexteFichierColonneNom + ", path=" + path + "]";
	}
	
	


}
