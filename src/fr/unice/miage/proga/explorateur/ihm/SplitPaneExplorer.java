/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unice.miage.proga.explorateur.ihm;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 *
 * @author michel
 */
public class SplitPaneExplorer extends JPanel{
    /*
     * PANEL DE GAUCHE DU SPLITPANE DEVANT CONTENIR L ARBORESECNCE DES DOSSIERS
     */
    private JPanel panelLeft;
    
    /*
     * PANNEL DE DROIT DU SPLITPANE DEVANT CONTENIR LE JTABLE POUR LES INFORMATIONS SUR LES DOSSIERS
     */
    private JPanel panelRight;
    
     private JSplitPane splitPane;

    public SplitPaneExplorer() 
    {
        this.panelLeft = new JPanel();
        this.panelRight= new JPanel();
        
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,panelLeft, panelRight);
        splitPane.setOneTouchExpandable(true);
        splitPane.setPreferredSize(new Dimension(400, 200));// DEFINITION DE LA DIMENSION DU SPLITPANE
        splitPane.setDividerLocation(200); // LARGEUR DU PANNEAU GAUCHE       
        
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }
    
    
    public JPanel getPanelLeft() {
        return panelLeft;
    }

    public JPanel getPanelRight() {
        return panelRight;
    }
    
    
    
    
    
}
