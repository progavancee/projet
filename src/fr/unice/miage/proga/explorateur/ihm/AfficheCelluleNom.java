package fr.unice.miage.proga.explorateur.ihm;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.unice.miage.proga.explorateur.ihm.TreeExplorer.FileNode;

public class AfficheCelluleNom extends DefaultTableCellRenderer{

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
			int column) {
		// TODO Auto-generated method stub
		if(value instanceof LabelPersonnalise){
	           //This time return only the JLabel without icon
			
			JLabel label = (LabelPersonnalise)value;
	            return label;
	        }
	 
	        else
	            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	}

	

}
