package fr.unice.miage.proga.explorateur.ihm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.*;

import fr.unice.miage.classloader.FileExplorerClassloader;
import fr.unice.miage.plugins.IPluginAnalyse;
import fr.unice.miage.plugins.IPluginVue;


@SuppressWarnings("serial")
public class MenuExploreur extends JMenuBar{
	

		private JMenuBar bar;
		private ArrayList<Class<?>> listePluginsVues;
		private ArrayList<Class<?>> listePluginsAnalyses;

		
		public MenuExploreur(){
			bar = new JMenuBar();
			buildMenu();
		}
	
		void buildMenu() {
	        JMenu fileM = new JMenu("Plugins");
	        bar.add(fileM);
	        fileM.add(new AbstractAction("Ajouter Plugin de Vue", new ImageIcon("res/save-icon16.png")) {
	            @Override
	            public void actionPerformed(ActionEvent arg0) {
	                System.out.println("DoSaveAction:"+arg0);
	                //en cours d'implementation
	        		JFileChooser jfc = new JFileChooser();
	        		jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
	        		jfc.setDialogTitle("Choisissez les plugins de VUE a charger");
	        		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        		if(jfc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
	        			String chemin = jfc.getSelectedFile().getAbsolutePath();
	        			System.out.println(chemin);
	        			FileExplorerClassloader fec = new FileExplorerClassloader(new File(chemin));
	        				//charge tout les .class .zip .jar du repertoire choisi
							fec.load();
							//liste avec tout les plugins de vues
							listePluginsVues=fec.loadPlugins(IPluginVue.class);
							//fec.getListeClass() pour recuperer la liste des classes chargees
							
							//boucle qui attache tout les plugins à la classe Fenetre
							for (Class<?> class1 : listePluginsVues) {
								if (class1!=null) {
					                try {
										((IPluginVue) class1.newInstance()).addPlugin((JFrame)bar.getTopLevelAncestor());
									} catch (InstantiationException | IllegalAccessException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}
	        		}

	            }
	            @Override
	            public Object getValue(String arg0) {
	                if(arg0==Action.ACCELERATOR_KEY) // cannot be changed later (use putValue when possible - not anonymous)
	                    return KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK);
	                return super.getValue(arg0);
	            }
	        });
	        
	        fileM.add(new AbstractAction("Ajouter Plugin Analyse", new ImageIcon("res/save-icon16.png")) {
	            @Override
	            public void actionPerformed(ActionEvent arg0) {
	                System.out.println("DoSaveAction:"+arg0);
	                //en cours d'implementation
	        		JFileChooser jfc = new JFileChooser();
	        		jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
	        		jfc.setDialogTitle("Choisissez les plugins d'analyse a charger");
	        		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        		if(jfc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
	        			String chemin = jfc.getSelectedFile().getAbsolutePath();
	        			System.out.println(chemin);
	        			FileExplorerClassloader fec = new FileExplorerClassloader(new File(chemin));
	        				//charge tout les .class .zip .jar du repertoire choisi
							fec.load();
							//liste avec tout les plugins de vues
							listePluginsAnalyses=fec.loadPlugins(IPluginAnalyse.class);
							//fec.getListeClass() pour recuperer la liste des classes chargees
							
							//boucle qui attache tout les plugins à la classe Fenetre
							for (Class<?> class1 : listePluginsAnalyses) {
								if (class1!=null) {
					                try {
										((IPluginAnalyse) class1.newInstance()).addPlugin((JFrame)bar.getTopLevelAncestor());
									} catch (InstantiationException | IllegalAccessException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}
	        		}

	            }
	            @Override
	            public Object getValue(String arg0) {
	                if(arg0==Action.ACCELERATOR_KEY) // cannot be changed later (use putValue when possible - not anonymous)
	                    return KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK);
	                return super.getValue(arg0);
	            }
	        });
	        
	        fileM.add(new AbstractAction("Configuration des plugins", new ImageIcon("res/save-icon16.png")) {
	            @Override
	            public void actionPerformed(ActionEvent arg0) {
	                
	            	ArrayList<Object[]> m = new ArrayList<Object[]>();
	            	
	            	if(listePluginsAnalyses != null)
		            	for (Class<?> class1 : listePluginsAnalyses) {
							if (class1!=null) {
				                try {
				                	IPluginAnalyse c = ((IPluginAnalyse) class1.newInstance());
				                	String[] d = {c.getName(),c.getDescription(),"Analyse"};
				                	m.add(d);
				                } catch (InstantiationException | IllegalAccessException e) {
									e.printStackTrace();
								}
							}
						}
	            	
	            	if(listePluginsVues != null)
		            	for (Class<?> class1 : listePluginsVues) {
							if (class1!=null) {
				                try {
				                	IPluginVue c = ((IPluginVue) class1.newInstance());
				                	String[] d = {c.getName(),c.getDescription(),"Vue"};
				                	m.add(d);
				                } catch (InstantiationException | IllegalAccessException e) {
									e.printStackTrace();
								}
							}
						}
	            	String[] name = {"nom","description","type"};
	            	Object[][] cells = new Object[m.size()][];	            	
	            	
	            	for (int i = 0; i < cells.length; i++) {
						cells[i] = m.get(i);
					}
	            	
	            	JTable table = new JTable(cells,name);
	            	JFrame frame = new JFrame("Fenetre de configuration de plugins");
	            	frame.setLayout(new BorderLayout());
	            	frame.add(new JScrollPane(table));
	                frame.pack();
	                frame.setLocationRelativeTo(null);
	                frame.setVisible(true); 

	            }
	           
	        });
	        
	        JMenu subMenu = new JMenu();
	        bar.add(subMenu);

	    }

		public JMenuBar getBar() {
			return bar;
		}

		public ArrayList<Class<?>> getListePluginsVues() {
			return listePluginsVues;
		}
		
		public ArrayList<Class<?>> getListePluginsAnalyses() {
			return listePluginsAnalyses;
		}


	
}
