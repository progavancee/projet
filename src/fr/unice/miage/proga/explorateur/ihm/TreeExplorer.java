/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unice.miage.proga.explorateur.ihm;

import java.io.File;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author michel
 */
public class TreeExplorer {
	
    private JTree tree;
    
    // Hame�on pour mettre � jour la table
    private TableModelExplorer tableExplorer;
    
    
   private DefaultMutableTreeNode root; //RACINE DE L'ARBRE

    public TreeExplorer() {	
    	root = new DefaultMutableTreeNode();
    	tree = new JTree(root);
    	
    	this.createMainNodeAndSubNode();
    	//JScrollPane treeView = new JScrollPane(tree);
    	tree.setRootVisible(false);
    	
    	// On ne peux s�l�ctionner qu'un seul noeud a la fois
    	tree.getSelectionModel().setSelectionMode
        (TreeSelectionModel.SINGLE_TREE_SELECTION);
    	
    	this.addFocusListener();
    	
    }

    public JTree getTree() {
        return tree;
    }
    
    public void createMainNodeAndSubNode()
    {
       
        File[] listRoots = File.listRoots();

        for (File mainNode : listRoots) 
        {
            if(mainNode.isDirectory())
            {System.out.println("file " +mainNode.getAbsoluteFile() + " liste dossier " +mainNode.listFiles().length);
                DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
                DefaultMutableTreeNode rootMain = (DefaultMutableTreeNode)model.getRoot();
                
                DefaultMutableTreeNode subRoot = new DefaultMutableTreeNode(new FileNode(mainNode).toString());
                CreateChildNodes ccn =new CreateChildNodes(mainNode, subRoot);
                new Thread(ccn).start();
                rootMain.add(subRoot);
                //rootMain.add(new DefaultMutableTreeNode(mainNode.getAbsoluteFile()));
                model.reload(rootMain);
            }
            
        }
    }
    
    /*
        CLASSE RETOURNANT LA DESCRIPTION DU NOEUD PARTICULIEREEMENT LE NOM
    */
    public class FileNode {

        private File file;

        public FileNode(File file) {
            this.file = file;
        }

        @Override
        public String toString() {
            String name = file.getName();
            if (name.equals("")) {
                return file.getAbsolutePath();
            } else {
                return name;
            }
        }
        
        public String getAbsolutePath(){
        	return this.file.getAbsolutePath();
        }
    }
    
    /*
       CREATION DES NOEUDS ENFANTS
    */
    
    public class CreateChildNodes implements Runnable {

        private DefaultMutableTreeNode root; // NOEUD PARENTS

        private File fileRoot;  // LISTE DES FICHIERS CONTENUS DANS LE PARENT

        public CreateChildNodes(File fileRoot, DefaultMutableTreeNode root) {
            this.fileRoot = fileRoot;
            this.root = root;
        }

        @Override
        public void run() {
            createChildren(fileRoot, root);
        }

        private void createChildren(File fileRoot, DefaultMutableTreeNode node) 
        {
        	
            File[] files = fileRoot.listFiles();
            if (files == null) return;

            for (File file : files) 
            {
                DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(new FileNode(file));
                node.add(childNode);
                if (file.isDirectory()) 
                {
                    createChildren(file, childNode);
                }
            }
        }

    }
    
    /***
     * Ajoute un �couteur qui se charge de mettre � jour les donn�es dans la table de droite
     * On peut am�liorer cette partie en externalisant ce traitement (pas facile parce que l'�couteur doit etre
     * plac� sur le Jtree...)
     */
    public void addFocusListener(){    	
    	
    	// Ajout d'un �couteur pour mettre � jour la table de droite
    	tree.addTreeSelectionListener(new TreeSelectionListener() {
    	    public void valueChanged(TreeSelectionEvent e) {
    	        DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();

    	        /* si rien n'est selectionn� et que la table n'est pas � jour */ 
    	        if (node == null && tableExplorer == null) 
    	        	return;

    	        /* On retrouve le noeud selectionn� */ 
    	        try{
    	        FileNode nodeInfo = (FileNode) node.getUserObject();
    	        
    	        //DEBUG
    	        //System.out.println("Noeud selected : " + nodeInfo.toString() + " " + nodeInfo.getAbsolutePath());   	        
    	        
    	        // On r�cup�re le chemin absolu du dossier
    	        String absolutePath = nodeInfo.getAbsolutePath();
    	        
    	        // On l'envoie � la table pour qu'elle se mette � jour
    	        tableExplorer.fillWithRepositoryData(absolutePath);  	        
    	        
    	        }catch(ClassCastException cce){
    	        	System.out.println("Le noeud racine ne peux pas etre d�taill�");
    	        }    	        
    	    }
    	});
    }
    
    /**
     * Ajout d'un hame�on sur la tableModelExplorer
     * @param tme
     */
    public void addTableModel(TableModelExplorer tme){
    	this.tableExplorer = tme;
    }
     
}
    
    
    
    
    

