/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unice.miage.proga.explorateur.ihm;

import java.io.File;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import fr.unice.miage.prog.interfaces.ContexteFichierColonneNom;
import fr.unice.miage.prog.interfaces.RepertoireImplTypeImageFichier;
import fr.unice.miage.prog.interfaces.ToutFichier;
import fr.unice.miage.proga.explorateur.utils.Utils;

/**
 *
 * @author michel
 */
public class TableModelExplorer extends AbstractTableModel{

    private String[] nomsColumns;
    
    private Object[][] donneesTable;
    
    private boolean DEBUG = false;

    public TableModelExplorer(String[] nomsColumns, Object[][] donneesTable) {
        this.nomsColumns = nomsColumns;
        this.donneesTable = donneesTable;
    }
    
    /*
     * retourne le tableau des noms des colonnes du table
     */
    public String[] getNomsColumns() {
        return nomsColumns;
    }

    public void setNomsColumns(String[] nomsColumns) {
        this.nomsColumns = nomsColumns;
    }

    /*
     * retourne les donn�es du tableau
     */
    public Object[][] getDonneesTable() {
        return donneesTable;
    }

    public void setDonneesTable(Object[][] donneesTable) {
        this.donneesTable = donneesTable;
    }
    
    /* Nombre de lignes du tableau except la ligne des noms des colonnes*/
    public int getRowCount() {
        return donneesTable.length;
    }

    public int getColumnCount() {
         return this.nomsColumns.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
         return donneesTable[rowIndex][columnIndex]; 
    }

    /*affecter les noms des colonnes du table*/
    @Override
    public String getColumnName(int column) {
        return this.nomsColumns[column]; 
    }
    
   
    
    /***
     * Met � jour les donn�es de la table avec les donn�es r�cup�r� depuis le path pass� en
     * param�tre
     * @param absolutePath chemin d'un dossier
     */
    public void fillWithRepositoryData(String absolutePath){
    	File target = new File(absolutePath);
    	System.out.println(this.getColumnClass(0));
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		ContexteFichierColonneNom contexteFichier;
		LabelPersonnalise labelPersonnalise =null;
		fireTableStructureChanged();
    	
    	//fireTableStructureChanged();
    	
    	// DEBUG
    	if(target.isDirectory())
    		System.out.println("Est un reprtoire");
    	else
    	{
    		System.out.println("N'est pas un repertoire");
    		this.setDonneesTable(new Object[1][4]);
    		
    		String extensionFichier = (Utils.getExtension(target.getName()) != "") ? Utils.getExtension(target.getName()) : "Inconnu";
    		// Last modified
    		this.donneesTable[0][1] = sdf.format(target.lastModified());
    		
    		 contexteFichier = new ContexteFichierColonneNom(new ToutFichier());
		     labelPersonnalise = new LabelPersonnalise(target.getName(), extensionFichier, contexteFichier,
		    		 target.getAbsolutePath());
    		 this.donneesTable[0][2] = (Utils.getExtension(target.getName()) != "") ? Utils.getExtension(target.getName()) : "Inconnu";
    		 this.donneesTable[0][0] = labelPersonnalise;
   
    		this.donneesTable[0][3] = Utils.humanReadableByteCount(target.length(), true);
    		fireTableStructureChanged();
    		return ;
    	}
    	
    	File[] files = target.listFiles();
    	int count = files.length;
    	System.out.println(count);
    	
    	this.donneesTable = new Object[count][4];
    	
    	for(int i = 0; i < count; i++){
    		String extensionFichier = (Utils.getExtension(files[i].getName()) != "") ? Utils.getExtension(files[i].getName()) : "Inconnu";
  
    		// Last modified
    		this.donneesTable[i][1] = sdf.format(files[i].lastModified());
    		
    		// Get extension
    		if(files[i].isDirectory())
    		{
    			 contexteFichier = new ContexteFichierColonneNom(new RepertoireImplTypeImageFichier());
    			 labelPersonnalise = new LabelPersonnalise(files[i].getName(), extensionFichier, contexteFichier, 
    					 files[i].getAbsolutePath());
    			this.donneesTable[i][2] = "Repertoire";
    		}else if(files[i].isFile())
    		{
    			contexteFichier = new ContexteFichierColonneNom(new ToutFichier());
   			    labelPersonnalise = new LabelPersonnalise(files[i].getName(), extensionFichier, 
   			    		contexteFichier, files[i].getAbsolutePath());
    			this.donneesTable[i][2] = extensionFichier;
    		}
    		
    		
    		this.donneesTable[i][0] = labelPersonnalise!=null?labelPersonnalise:files[i].getName();
    		// Get lenght
    		this.donneesTable[i][3] = Utils.humanReadableByteCount(files[i].length(), true);
    	}
    	
    	//fireTableRowsInserted(0, getRowCount());
    	fireTableStructureChanged();
    	
    }
    
    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class getColumnClass(int column)
    {
        for (int row = 0; row < getRowCount(); row++)
        {
            Object o = getValueAt(row, column);

            if (o != null)
            {
            	//System.out.println(o.getClass());
                return o.getClass();
            }
        }
        //System.out.println(Object.class);
        return Object.class;
    }
 
    
}
