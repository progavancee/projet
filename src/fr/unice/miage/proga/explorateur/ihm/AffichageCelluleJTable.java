package fr.unice.miage.proga.explorateur.ihm;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class AffichageCelluleJTable extends JLabel implements TableCellRenderer {

	private Border unselectedBorder = null;
    private Border selectedBorder = null;
    private boolean isBordered = true;

    public AffichageCelluleJTable() {
        
        setOpaque(true); //MUST do this for background to show up.
    }

    public Component getTableCellRendererComponent(
                            JTable table, Object label,
                            boolean isSelected, boolean hasFocus,
                            int row, int column) {
        JLabel jLabel = (JLabel)label;
        return this;
    }
	
	

}
