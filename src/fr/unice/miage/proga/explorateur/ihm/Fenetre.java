/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unice.miage.proga.explorateur.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.unice.miage.classloader.PanelEditorColumn;
import fr.unice.miage.prog.interfaces.ContexteFichierColonneNom;
import fr.unice.miage.prog.interfaces.ITypeImageFichier;
import fr.unice.miage.prog.interfaces.RepertoireImplTypeImageFichier;
import fr.unice.miage.proga.lookFell.ApparenceBaseNoeud;

/**
 *
 * @author michel
 */
public class Fenetre extends JFrame{
	/*
	 * SPLIT PANE POUR DIVISER LA FENETERE EN DEUX
	 */
    private SplitPaneExplorer splitExplorateur;
    
    /*
     * TREE EXPLORER POUR LA PRESENTATION DES DOSSIERS DANS L'ARBORESECENCE
     */
    private TreeExplorer treeExplorer;
    
    private JTablePanelDroit table;// JTABLE POUR LE PANNELDROIT
    
    private TableModelExplorer tableModelTable; //composant contenant les donn�es du Table
    
    private String[] nomsColumns ={"Nom", "Modifie le", "Type", "Taille"}; // noms des columns
    
    private Object[][] donneesTable;//donn�es du tableau
    
    private JScrollPane scrollPaneTable; // bare de d�filement du Jtable
    
    private JMenuBar menuBar;

    public Fenetre(String title) throws HeadlessException {
        super(title);
        this.splitExplorateur = new SplitPaneExplorer();
        this.getContentPane().add(this.splitExplorateur.getSplitPane());
        menuBar = new MenuExploreur().getBar();
        
        treeExplorer = new TreeExplorer();
        
        /*
         * DEFINITION DU LAYOUT POUR LE PANEL GAUCHE DU SPLIT PANE
         */
        this.splitExplorateur.getPanelLeft().setLayout(new GridLayout(1,0));
        this.splitExplorateur.getPanelLeft().add(new JScrollPane(this.treeExplorer.getTree()));// appel du tree
        this.treeExplorer.getTree().setCellRenderer(new ApparenceBaseNoeud(
        		new ImageIcon("images/toutFichier2.gif")));
        
        /*
         * ELEMENT PANEL DROIT DE L'EXPLORATEUR
         */
        donneesTable = new Object[0][4]; // JUSTE FICTIF EN ANTENDANT LES DONNEES
        
        ContexteFichierColonneNom typeFichier = new ContexteFichierColonneNom(new RepertoireImplTypeImageFichier());
       
        //donneesTable[0][0]=label; donneesTable[0][1]="cc";donneesTable[0][2]="cc";donneesTable[0][3]="cc";
        this.splitExplorateur.getPanelRight().setLayout(new GridLayout(1,0));
        this.tableModelTable = new TableModelExplorer(nomsColumns, donneesTable);
        this.treeExplorer.addTableModel(tableModelTable);
        this.table = new JTablePanelDroit(tableModelTable);
        
      
        table.setDefaultRenderer(LabelPersonnalise.class, new AfficheCelluleNom());//appliquer une rendu sur les element JTable
        table.setDefaultRenderer(LabelPersonnalise.class, new RenduCelluleNom());//pour rendre la cellule editable 
        
        this.scrollPaneTable = new JScrollPane(table);
        this.splitExplorateur.getPanelRight().add(scrollPaneTable);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(menuBar);
        this.setVisible(true);
        this.pack();
        
        /*
         *  ECOUTEUR POUR NAVIGUER DANS LE JTABLE
         */
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = table.rowAtPoint(evt.getPoint());
                int col = table.columnAtPoint(evt.getPoint());
                if (row >= 0 && col >= 0) {
                   //System.out.println( row  +" " + col);
                	LabelPersonnalise l = (LabelPersonnalise) table.getValueAt(table.getSelectedRow(), col);
                	String type = l.getTypeFichier();
                	String path = l.getPath();
                	
                	//System.out.println(" fichier " +l.getTypeFichier() +" absolute "+path);
                	if(type.equals("Inconnu"))
                	{
                		
                		TableModelExplorer tableModelExplorer = (TableModelExplorer) table.getModel();
                		tableModelExplorer.fillWithRepositoryData(path);
                	}
                  
                }
            }
        });
	
    }
    
    
    
    
    
    public static void main(String[] args) {
        Fenetre fen = new Fenetre("Explorer");
    }    
    
}
