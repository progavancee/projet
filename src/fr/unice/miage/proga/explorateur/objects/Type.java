package fr.unice.miage.proga.explorateur.objects;

/***
 * Enumeration de tous les types de fichiers support�
 * Utilis� pour les icones
 * @author Nicolas
 *
 */
public enum Type {
	txt, jpg, jpeg, avi, folder
}
