import java.awt.Component;
import java.awt.Container;

import javax.swing.JFrame;

import fr.unice.miage.plugins.IPluginAnalyse;


public class PluginAnalyse1 implements IPluginAnalyse{
	
	private JFrame fenetre;
	private JFrame fenetreInitial;

	@Override
	public void plug() {
		System.out.println("PluginAnalyse1 is plugged");
	}

	@Override
	public void unplug() {
		System.out.println("PluginAnalyse1 is unplugged");
	}

	@Override
	public String getName() {
		return "PluginAnalyse1";
	}

	@Override
	public String getDescription() {
		return "Description du plugin";
	}

	@Override
	public boolean getEtat() {
		return false;
	}

	@Override
	public void activateIt() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void desactivateIt() {
		// TODO Auto-generated method stub
		
	}


	
	public void initPlugin(){
		Container c = this.fenetre.getContentPane();
		Component tab[] = c.getComponents();
		for (Component component : tab) {
			System.out.println(component.getName()+" "+component.getClass().getName());
		}
		
	}

	@Override
	public void addPlugin(JFrame fenetre) {
		this.fenetre = fenetre;
		this.fenetreInitial = fenetre;
		initPlugin();
	}

}
