import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

import fr.unice.miage.plugins.IPluginVue;


/*
 * Un plugin de vue affichant l'heure et la date dans l'explorateur"
 */
public class PluginVueHorloge implements IPluginVue{

	private JFrame fenetre;
	private JFrame fenetreInitial;

	@Override
	public void plug() {
		System.out.println("PluginVueHorloge is plugged");
	}

	@Override
	public void unplug() {
		System.out.println("PluginVueHorloge is unplugged");
	}

	@Override
	public String getName() {
		return "PluginVueHorloge";
	}

	@Override
	public String getDescription() {
		return "Un plugin de vue affichant l'heure et la date dans l'explorateur";
	}

	@Override
	public boolean getEtat() {
		return false;
	}

	@Override
	public void activateIt() {
		// TODO Auto-generated method stub
		//initPlugin();
	}

	@Override
	public void desactivateIt() {
		// TODO Auto-generated method stub
		//this.fenetre=fenetreInitial;
	}

	@Override
	public void addPlugin(JFrame fenetre) {
		this.fenetre = fenetre;
		this.fenetreInitial = fenetre;
		initPlugin();
	}
	
	public void initPlugin(){
		final JLabel tijd;
		final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
        tijd = new JLabel(dateFormat.format(now.getTime()));
        tijd.setBounds(100, 100, 125, 125);

		 new Timer(1000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Calendar now = Calendar.getInstance();
	                tijd.setText(dateFormat.format(now.getTime()));
				}
	        }).start();
		
		 this.fenetre.add(tijd,BorderLayout.SOUTH);
         this.fenetre.pack();
       
	}

}
